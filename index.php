<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration System</title>
    <!--css-->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Bootstrap Css-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!--Bootstrap bundle with popper-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!--fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Courgette&display=swap" rel="stylesheet"> 
    <!---favicon link--->
    <link rel="icon" href="img/2561496_user_icon.png" type="image/x-icon" >
    <!---ajax--->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/egg.js/1.0/egg.min.js" integrity="sha512-tCSilflSXbmjMtEv9NTrhg32Lnv7zkoiiBa4QuKUvHeCEovI705gfG15UKyUCm2OZMLTahszyFyedeXbnbdXKg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> 

</head>
<body>

    <!---Heading block-->
    <div class="container-fluid" id="headings">
        <h1><strong>User Registration System</strong></h1>
        <h3 style="color: #1e1f1f;"> Sample Project 1</h3>
    </div>

    <!--navigation bar-->
    <nav class="navbar navbar-expand-lg navbar-light" style="background-image: linear-gradient(-225deg, #ebf7ff 0%, #f2f2f2, #f9fff0 100%);">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expended="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php"><b>HOMEPAGE </b></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="display_user.php">Registered Users</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                    </li>
                </ul>
                <li class="d-flex">
                    <a href="login.php"><button class="btn btn-outline-success me-2" type="submit">Login</button></a>
                </li>
                <li class="d-flex">
                    <a href="signup.php"><button class="btn btn-outline-success me-2" type="submit">Register</button></a>
                </li>
            </div>
        </div>
    </nav>

    <div id="main-para" style="padding-left: 7%; padding-right: 7%;">
        <h2><b>Welcome!</b></h2>
    <br>
    <ul>
        <li>Sign Up</li>
        <li>Or Register As New User.</li>
    </ul>
    <br>
    <!--boxes-->
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                <h5 class="card-title"><b>Users</b></h5>
                <p class="card-text">View List Of Registered Users</p>
                <a href="display_user.php" class="btn btn-primary">Go</a>
            </div>
        </div>
    </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><b>Register</b></h5>
                    <p class="card-text">Register as New User</p>
                    <a href="signup.php" class="btn btn-primary">Take Me There</a>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br><br><br>
    </div>
    <footer class="foot">
        <div><p style="margin: 0px; padding: 5px;">Thanks For Visiting!</p></div>
    </footer>
</body>
</html>